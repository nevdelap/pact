#!/usr/bin/python
import atexit
import requests
import unittest

from pact import Consumer, EachLike, Like, Provider, Term
from pact.matchers import get_generated_values


pact = Consumer('Test consumer').has_pact_with(
    Provider('User info test provider'), host_name='localhost', port=8080, pact_dir='pacts')
pact.start_service()
atexit.register(pact.stop_service)


class GetUserInfoContract(unittest.TestCase):
    def test_get_user(self):
        expected = {
            'result': 'success',
            'username': Term('[a-z]+', 'nevd'),
            'id': Like(456),
            'groups': EachLike('group')
        }

        (pact
         .given('nevd exists')
         .upon_receiving('a request for nevd')
         .with_request('GET', '/users/nevd')
         .will_respond_with(200, body=expected))

        with pact:
            uri = 'http://localhost:8080/users/nevd'
            result = requests.get(uri).json()

        self.assertEqual(result, get_generated_values(expected))

    def test_fail_to_get_user(self):
        (pact
         .given('billb doesn\'t exist')
         .upon_receiving('a request for billb')
         .with_request('GET', '/users/billb')
         .will_respond_with(404))

        with pact:
            uri = 'http://localhost:8080/users/billb'
            requests.get(uri)
