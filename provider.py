#!/usr/bin/python
import json
import web

urls = (
    '/', 'index',
    '/users/nevd', 'users'
)


class index:
    def GET(self):
        return web.template.render('templates/').index()


class users:
    def GET(self):
        user = {
            'result': 'success',
            'username': 'nevd',
            'id': 123,
            'groups': ['drones']
        }
        web.header('Content-Type', 'application/json')
        return json.dumps(user)


if __name__ == "__main__":
    web.template.ALLOWED_AST_NODES.append('Constant')
    app = web.application(urls, globals())
    app.run()
