# Pact Fiddling

## Setup

`sudo pip install pact-python`  
`sudo pip install web.py`

## Stuff

`provider.py` - Exposes http://localhost:8080/users/nevd, returns 404 for any other user id.  
`consumer.py` - Runs against it, asserting it returns the right things.  
`contract_test.py` - Contract test that writes a pact file.

## Creating the Pact File

```
python -m unittest contract_test.py
```

That writes out `pacts/test_consumer-user_info_test_provider.json`.

## Running the Local Stub

```
docker run --name localstub -p 8080:8080 -v "$(pwd)/pacts:/app/pacts" -d pactfoundation/pact-stub-server -p 8080 --dir pacts
```

which is also in:

```
./localstub
```

Stop it with:

```
docker rm -f localstub
```

## Testing The Contract Against the Real Provider

In one shell:

```
./provider.py
```

In another:

```
pact-verifier --provider-base-url=http://localhost:8080 --pact-url=./pacts/test_consumer-user_info_test_provider.json
```

which is also in:

```
./verify
```

You can run the pact-verifier against the local stub too.
