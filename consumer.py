#!/usr/bin/python
import requests

username = 'nevd'
uri = 'http://localhost:8080/users/' + username
response = requests.get(uri)
assert(response.status_code == 200)
print(response.json())

username = 'billb'
uri = 'http://localhost:8080/users/' + username
assert(requests.get(uri).status_code == 404)
